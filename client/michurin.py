#!/usr/bin/env python

import os
import sys
# import json
from datetime import datetime as dt
from datetime import timedelta

from dateutil import parser
import click
import requests
from requests.exceptions import MissingSchema
import pytz
import pandas as pd
from tabulate import tabulate
import plotille
from dotenv import load_dotenv

import pdb

# Load environment
env_path = os.path.join(os.path.dirname(sys.argv[0]), ".env")
load_dotenv(dotenv_path=env_path)

# server access
MICHURIN_TOKEN = os.getenv("MICHURIN_TOKEN")
MICHURIN_ENDPOINT = os.getenv("MICHURIN_ENDPOINT")


def fetch_plant_info():
	"""
	"""
	token = MICHURIN_TOKEN
	endpoint = MICHURIN_ENDPOINT + "/"
	headers = {"Authorization": token}
	res = requests.get(endpoint, headers=headers)

	assert res.status_code == 200
	info = pd.DataFrame(res.json()['sensors'])

	return info


def fetch_latest(plant: int):
	"""
	"""

	token = MICHURIN_TOKEN
	endpoint = MICHURIN_ENDPOINT + f"/{plant}/data/latest"
	headers = {"Authorization": token}
	res = requests.get(endpoint, headers=headers)

	assert res.status_code == 200
	res_json = res.json()

	# value = res_json['data']['value']
	moistureRelative = res_json['data']['moistureRelative']
	moistureAbsolute = res_json['data']['moistureAbsolute']
	AirValue = res_json['data']['AirValue']
	WaterValue = res_json['data']['WaterValue']
	timestamp = parser.parse(res_json['data']['timestamp'])

	return moistureRelative, moistureAbsolute, AirValue, WaterValue, timestamp


def fetch_all(plant: int):
	"""
	Fetch the sensor data from the server.

	Returns
	-------
	data : pandas.DataFrame
		Moisture level values.
	"""

	token = MICHURIN_TOKEN
	endpoint = MICHURIN_ENDPOINT + f"/{plant}/data"
	headers = {"Authorization": token}
	res = requests.get(endpoint, headers=headers)

	assert res.status_code == 200
	res_json = res.json()

	data = pd.DataFrame(res_json['data'])
	cols = [
		'moistureRelative', 'moistureAbsolute', 
		'AirValue', 'WaterValue', 'timestamp', 'sensor_id'
	]
	assert all(c in data.columns for c in cols)
	data['timestamp'] = pd.to_datetime(data['timestamp'])
	data['timestamp'] = data['timestamp'].dt.tz_localize(None)
	data.set_index('timestamp', inplace=True)
	data.sort_index(inplace=True)

	return data


def parse_plant_id(plant, info):
	"""
	"""
	try:
		plant_id = int(plant)
		plant_record = info[info['id'] == plant_id]
	except ValueError:
		plant_record = info[info['name'] == plant]
		plant_id = int(plant_record['id'])

	return plant_id, plant_record



@click.group()
def main():
	pass


@main.command()
def plants(**kwargs):
	"""
	Show info about available plants.
	"""
	try:
		info = fetch_plant_info()
	except Exception as e:
		raise e

	print(tabulate(
		info, 
		headers="keys",
		tablefmt="fancy_grid",
		showindex=False
	))



@main.command()
@click.argument("plant")
def plot(plant):
	"""
	Fetch the entire dataset for a given sensor, and plot it in the terminal.
	"""
	print("Downloading data...\n")
	info = fetch_plant_info()

	plant_id, plant_record = parse_plant_id(plant, info)

	print(tabulate(
		plant_record, 
		headers="keys", 
		tablefmt="fancy_grid",
		showindex=False
	))

	df = fetch_all(plant_id)
	data = df['moistureRelative']
	interval = "10Min"

	# Interval boundaries need to be normalized ("snapped" to a round
	# timestamp), otherwise they will not match with the index created
	# by the resampler. There is a "normalize" argument in date_range(), but
	# it only has 1 day resolution (snaps to midnight). To fix that, we need
	# to snap manually.
	index = pd.date_range(
		data.index.min()
			.replace(minute=0, second=0, microsecond=0), 
		(data.index.max() + pd.Timedelta(hours=1))
			.replace(minute=0, second=0, microsecond=0), 
		freq=interval
	)

	resampled = (
		data
			.resample(interval)
			.mean()
			.reindex(index)
			.interpolate()  # linear
			.dropna()
	)

	print()
	fig = plotille.Figure()
	fig.width = 64
	fig.height = 20
	fig.set_y_limits(min_=0, max_=1)
	fig.color_mode = 'rgb'
	fig.plot(
		X=resampled.index, 
		Y=resampled.values
	)
	print(fig.show())
	print()


@main.command()
@click.argument("plant")
def now(plant):
	"""
	"""
	info = fetch_plant_info()
	plant_id, _ = parse_plant_id(plant, info)

	try:
		print("Downloading data")
		(
			moistureRelative, moistureAbsolute, 
			AirValue, WaterValue, timestamp
		) = fetch_latest(plant_id)
	except Exception as e:
		raise e

	print(moistureRelative, timestamp.isoformat())


if __name__ == "__main__":
	main()
