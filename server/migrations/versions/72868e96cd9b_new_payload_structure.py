"""new payload structure

Revision ID: 72868e96cd9b
Revises: bf35ce638f0f
Create Date: 2021-01-26 17:03:51.580655

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '72868e96cd9b'
down_revision = 'bf35ce638f0f'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
