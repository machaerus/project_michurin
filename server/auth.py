import os
from functools import wraps

from flask import request

from utils import api_error


def restricted(func):
	"""
	TODO:

	Implement some proper auth!
	"""
	@wraps(func)
	def wrapper(*args, **kwargs):
		fname = func.__name__
		expected = os.environ.get("API_PASS")
		password = request.headers.get("Authorization")
		if password == expected:
			return func(*args, **kwargs)
		else:
			api_error(403, "Unauthorized!")
	return wrapper