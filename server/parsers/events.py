from flask import g
from datetime import datetime, timedelta
from flask_restplus.namespace import RequestParser
import dateutil.parser
from utils import api_error


EventsList_parser = RequestParser()
EventsList_parser.add_argument(
	"start_time",
	location="args",
	help="Use ISO 8601 time strings. Defaults to 1 hours before now."
)
EventsList_parser.add_argument(
	"end_time",
	location="args",
	help="Use ISO 8601 time strings. Defaults to now."
)


def EventsList_parse_arguments(req_args):
	"""."""
	try:
		end_time = req_args.get("end_time")\
			or datetime.utcnow()
		start_time = req_args.get("start_time")\
			or end_time - timedelta(hours=1)

		end_time = dateutil.parser.parse(
			str(end_time)
		)
		start_time = dateutil.parser.parse(
			str(start_time)
		)

		g.start_time = start_time
		g.end_time = end_time

	except ValueError:
		api_error(400, "Invalid date format.")
		