from datetime import datetime as dt
import logging

from flask import jsonify, request
from flask_restplus import fields, Resource, Namespace
from flask import g
from sqlalchemy import desc

from models import db, Sensor, Event
from utils import api_error
from parsers.events import (
	EventsList_parser,
	EventsList_parse_arguments,
)
from auth import restricted


logger = logging.getLogger('michurin_logger')


api = Namespace(
	"sensors",
	description="Sensors"
)


SensorModel = api.model(
	"Sensor",
	dict(
		id=fields.Integer(
			required=False, description=""),
		name=fields.String(
			required=True, description=""),
		display_name=fields.String(
			required=True, description=""),
		sensor_type=fields.String(
			required=True, description="")
	)
)

EventModel = api.model(
	"Event",
	dict(
		id=fields.Integer(
			required=False, description=""),
		# value=fields.Float(
		# 	required=True, description=""),
		moistureAbsolute=fields.Float(
			required=True, description=""),
		moistureRelative=fields.Float(
			required=True, description=""),
		AirValue=fields.Float(
			required=True, description=""),
		WaterValue=fields.Float(
			required=True, description=""),
		timestamp=fields.DateTime(
			required=True, description=""),
		sensor_id=fields.Integer(
			required=False, description="")
	)
)


@api.route("/")
class SensorsList(Resource):
	"""."""

	@restricted
	@api.marshal_list_with(SensorModel, envelope="sensors")
	def get(self):
		return Sensor.query\
			.all()

	@restricted
	@api.expect(SensorModel)
	@api.marshal_with(SensorModel, code=201)
	def post(self):
		json_data = request.get_json()
		new_sensor = Sensor(
				name=json_data['name'],
				display_name=json_data['display_name'],
				sensor_type=json_data['sensor_type']
			)
		db.session.add(new_sensor)
		db.session.commit()
		return new_sensor, 201


@api.route("/<sensor_id>")
@api.param("sensor_id", "Sensor ID")
class SensorSingle(Resource):
	"""."""

	@restricted
	@api.marshal_with(SensorModel)
	def get(self, sensor_id):
		return Sensor.query\
			.filter_by(id=sensor_id)\
			.one()

	@restricted
	@api.expect(SensorModel)
	@api.marshal_with(SensorModel, code=200)
	def put(self, sensor_id):
		"""
		This should be UPDATE instead, but apparently there is no UPDATE
		method supported in Flask Rest+. This is not a real PUT,
		it is not idempotent.
		"""
		sensor = Sensor.query\
			.filter_by(id=sensor_id)\
			.one()
		json_data = request.get_json()

		if "name" in json_data.keys():
			sensor.name = json_data['name']
		if "display_name" in json_data.keys():
			sensor.display_name = json_data['display_name']
		if "sensor_type" in json_data.keys():
			sensor.sensor_type = json_data['sensor_type']

		db.session.commit()
		return sensor, 200


@api.route("/<sensor_id>/data")
@api.param("sensor_id", "Sensor ID")
class EventsList(Resource):
	"""."""

	@restricted
	@api.expect(EventModel)
	@api.marshal_with(EventModel, code=201)
	def post(self, sensor_id):
		
		logger.info("Received a POST request!")
		logger.info(request.headers)
		logger.info(request.data)
		try:
			json_data = request.get_json()
			logger.info(json_data)
			new_event = Event(
					# value=json_data['value'], 
					moistureAbsolute=json_data['moistureAbsolute'], 
					moistureRelative=json_data['moistureRelative'], 
					AirValue=json_data['AirValue'], 
					WaterValue=json_data['WaterValue'], 
					# timestamp=json_data['timestamp'],
					timestamp=dt.utcnow().isoformat(),
					sensor_id=sensor_id
				)
			db.session.add(new_event)
			db.session.commit()
			return new_event, 201
		except Exception as e:			
			logger.exception(e)
			api_error(400, "Payload invalid")


	@restricted
	@api.marshal_list_with(EventModel, envelope="data")
	@api.expect(EventsList_parser)
	def get(self, sensor_id):
		"""."""
		EventsList_parse_arguments(
			EventsList_parser.parse_args(strict=True)
		)

		start_time = g.start_time
		end_time = g.end_time

		sensor_record = Sensor.query\
			.filter_by(id=sensor_id)\
			.one()

		if not sensor_record:
			api_error(400, "Sensor does not exist.")
		
		data = Event.query\
			.filter(
				Event.sensor_id == sensor_id,
				# Event.timestamp <= end_time,
				# Event.timestamp >= start_time
			)\
			.order_by(desc(Event.timestamp))\
			.all()

		return data


@api.route("/<sensor_id>/data/latest")
@api.param("sensor_id", "Sensor ID")
class EventLatest(Resource):
	"""."""

	@restricted
	@api.marshal_with(EventModel, envelope="data")
	def get(self, sensor_id):
		"""."""

		sensor_record = Sensor.query\
			.filter_by(id=sensor_id)\
			.one()

		if not sensor_record:
			api_error(400, "Sensor does not exist.")
		
		data = Event.query\
			.filter(
				Event.sensor_id == sensor_id,
				# Event.timestamp <= end_time,
				# Event.timestamp >= start_time
			)\
			.order_by(desc(Event.timestamp))\
			.first()

		return data


@api.route("/<sensor_id>/data/<event_id>")
@api.param("sensor_id", "Sensor ID")
@api.param("event_id", "Event ID")
class EventSingle(Resource):
	"""."""

	@restricted
	@api.marshal_with(EventModel, envelope="data")
	def get(self, sensor_id, event_id):
		"""."""

		sensor_record = Sensor.query\
			.filter_by(id=sensor_id)\
			.one()

		if not sensor_record:
			api_error(400, "Sensor does not exist.")
		
		data = Event.query\
			.filter_by(id=event_id)\
			.one()

		return data

	@restricted
	@api.marshal_with(EventModel, envelope="data")
	def delete(self, sensor_id, event_id):
		"""."""

		sensor_record = Sensor.query\
			.filter_by(id=sensor_id)\
			.one()

		if not sensor_record:
			api_error(400, "Sensor does not exist.")
		
		event_record = Event.query\
			.filter_by(id=event_id)\
			.one()

		db.session.delete(event_record)
		db.session.commit()

		return event_record
