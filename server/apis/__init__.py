from flask_restplus import Api
from apis.sensors import api as sensors
# from apis.events import api as events

api = Api(
	title="Project Michurin API",
	version="0.2",
	description=""" . """
)

api.add_namespace(sensors)
# api.add_namespace(events)
