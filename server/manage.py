from flask_script import Manager
from flask_migrate import MigrateCommand
from flask_dotenv import DotEnv
from app import application, db

env = DotEnv()
env.init_app(application, verbose_mode=True)

manager = Manager(application)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()