#!/usr/bin/env python

import os
import sys
import logging

from flask import Flask
from flask_migrate import Migrate
from flask_cors import CORS
# from flask_restplus import Resource, Api
# from flask_dotenv import DotEnv

from models import db
from apis import api

app_path = os.path.dirname(sys.argv[0])

logging.basicConfig(level=logging.DEBUG)
logname = os.path.join(os.path.dirname(sys.argv[0]), "app.log")
log_format = logging.Formatter(
	'[%(asctime)s] [%(levelname)s] %(message)s',
	'%Y-%m-%dT%H:%M:%S%z')

logger = logging.getLogger('michurin_logger')

fl = logging.FileHandler(logname)
fl.setFormatter(log_format)
fl.setLevel(logging.DEBUG)
logger.addHandler(fl)

stdl = logging.StreamHandler(sys.stdout)
stdl.setFormatter(log_format)
stdl.setLevel(logging.INFO)
logger.addHandler(stdl)


app = Flask(__name__)

# env = DotEnv()
# env.init_app(app)

cors = CORS(app, resources={r"/*": {"origins": "*"}})

app.config['PGUSER'] = os.environ.get("PGUSER")
app.config['PGPASSWORD'] = os.environ.get("PGPASSWORD")
app.config['PGHOST'] = os.environ.get("PGHOST")
app.config['PGDATABASE'] = os.environ.get("PGDATABASE")
app.config['SQLALCHEMY_DATABASE_URI'] = \
	os.environ.get("SQLALCHEMY_DATABASE_URI")

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
api.init_app(app)


# run the app.
if __name__ == "__main__":
	app.debug = True
	app.run("0.0.0.0")
else:
	logger.info(__name__)
	migrate = Migrate(app, db)
	application = app