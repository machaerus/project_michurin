from flask_restplus import abort

def api_error(code, message=None):
	"""json-api compliant error messages for api."""
	return abort(
		code,
		error=dict(
			status=code,
			detail=message
		)
	)