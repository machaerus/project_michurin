# import bcrypt
# from datetime import datetime as dt
# from datetime import timedelta
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import (
	Column, Integer, Float, 
	String, DateTime, Boolean, 
	ForeignKey
)
from sqlalchemy.dialects.postgresql import TIMESTAMP

db = SQLAlchemy()


class Sensor(db.Model):
	"""
	"""
	__tablename__ = 'sensors'

	id = Column(Integer, autoincrement=True, primary_key=True)
	name = Column(String, nullable=False, unique=True)
	display_name = Column(String, nullable=False)
	sensor_type = Column(String, nullable=False)


class Event(db.Model):
	"""
	"""
	__tablename__ = 'events'

	id = Column(Integer, autoincrement=True, primary_key=True)
	# value = Column(Float, nullable=False)
	moistureAbsolute = Column(Float)
	moistureRelative = Column(Float)
	AirValue = Column(Float)
	WaterValue = Column(Float)
	timestamp = Column(TIMESTAMP(timezone=True), nullable=False)
	sensor_id = Column(
		Integer, ForeignKey("sensors.id"), nullable=False)


# class User(db.Model):
# 	""" User Model for storing user related details """

# 	__tablename__ = "users"

# 	id = Column(Integer, primary_key=True, autoincrement=True)
# 	email = Column(String(255), unique=True, nullable=False)
# 	password = Column(String(255), nullable=False)
# 	registered_on = Column(TIMESTAMP(timezone=True), nullable=False)
# 	admin = Column(Boolean, nullable=False, default=False)

# 	def __init__(self, email, password, admin=False):
# 		self.email = email
# 		self.password = bcrypt.hashpw(
# 			password, bcrypt.gensalt(rounds=16)
# 		).decode()
# 		self.registered_on = dt.now()
# 		self.admin = admin

# 	def encode_auth_token(self, user_id):
# 		"""
# 		Generate a JWT token.

# 		Returns
# 		-------
# 		token : string
# 		"""
# 		try:
# 			now = dt.utcnow()
# 			payload = {
# 				'exp': now + timedelta(days=0, seconds=5),
# 				'iat': now,
# 				'sub': user_id
# 			}
# 			return jwt.encode(
# 				payload,
# 				app.config.get('SECRET_KEY'),
# 				algorithm='HS256'
# 			)
# 		except Exception as e:
# 			return e

# 	@staticmethod
# 	def decode_auth_token(auth_token):
# 		"""
# 		Decode the JWT token.

# 		Parameters
# 		----------
# 		auth_token
		
# 		Returns
# 		-------
# 		integer|string
# 		"""
# 		try:
# 			payload = jwt.decode(
# 				auth_token, app.config.get('SECRET_KEY'))
# 			return payload['sub']
# 		except jwt.ExpiredSignatureError:
# 			return 'Signature expired. Please log in again.'
# 		except jwt.InvalidTokenError:
# 			return 'Invalid token. Please log in again.'

