# Project Michurin

Automated plant watering and remote monitoring & management system.

## Firmware

Arduino C++ firmware. The circuit consists of a wifi-enabled development board, a capacitive moisture sensor, and a water pump.

To deploy the code:

`pio run -t upload`

To debug:

`pio device monitor`

## Server

Based on Flask RestPlus and PostgreSQL with alembic.

Create a new migration after changing the database model:

`env/bin/python manage.py db revision` - will create a revision file

`env/bin/python manage.py db migrate`

`env/bin/python manage.py db upgrade head` - will upgrade the database

Deploy:

`ansible-playbook deploy.yml` - will deploy the source code (incl. migrations)

Run:

`./run.sh`

On the server it is run automatically, use `systemctl` to manage the service and `psql` to manage the database.

## Client

Command line client for displaying and monitoring sensor data.

Use a virtual environment (obviously). After activating the environment, just run `python michurin.py` to explore the available options.

Add something like the following **example** script to your PATH for a more convenient use:

```
#!/bin/bash
"$HOME/.conda/envs/michurin/bin/python" "$HOME/path/to/your/project_michurin/client/michurin.py" "$@"
```