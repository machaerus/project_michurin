#include <SPI.h>
#include <WiFi101.h>
#include <ArduinoJson.h>

#include "secrets.h"

char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;
char server[] = SERVER_URL;
char api_pass[] = API_PASS;
int status = WL_IDLE_STATUS; // the WiFi radio's status
WiFiClient client;

// empirical values
const float AirValue = 880.0;
const float WaterValue = 480.0;
float amplitude = AirValue - WaterValue;

float moistureValue;
float moistureRelative;


void printMacAddress(byte mac[]) {
	for (int i = 5; i >= 0; i--) {
		if (mac[i] < 16) {
			Serial.print("0");
		}
		Serial.print(mac[i], HEX);
		if (i > 0) {
			Serial.print(":");
		}
	}
	Serial.println();
}

void printWiFiData() {
	// print your WiFi shield's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print your MAC address:
	byte mac[6];
	WiFi.macAddress(mac);
	Serial.print("MAC address: ");
	printMacAddress(mac);
}

void printCurrentNet() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print the MAC address of the router you're attached to:
	byte bssid[6];
	WiFi.BSSID(bssid);
	Serial.print("BSSID: ");
	printMacAddress(bssid);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.println(rssi);

	// print the encryption type:
	byte encryption = WiFi.encryptionType();
	Serial.print("Encryption Type:");
	Serial.println(encryption, HEX);
	Serial.println();
}

void connectWifi() {
	// attempt to connect to WiFi network:
	while ( status != WL_CONNECTED ) {
		Serial.print("Attempting to connect to WPA SSID: ");
		Serial.println(ssid);
		// Connect to WPA/WPA2 network:
		status = WiFi.begin(ssid, pass);

		delay(5000);
	}

	// you're connected now, so print out the data:
	Serial.println("You're connected to the network");
}

void sendData(String payload) {
	if ( client.connect(server, 80) ) {
		Serial.println("Connected to server, sending data");

		client.println("POST /sensors/5/data HTTP/1.1");
		client.print("Host: ");
		client.println(server);
		client.println("User-Agent: Arduino/1.0");
		// client.println("Connection: close");
		client.println("Accept: application/json");
		client.print("Authorization: ");
		client.println(API_PASS);
		// client.println("Connection: keep-alive");
		client.println("Content-Type: application/json");
		client.print("Content-Length: ");
  		client.println(payload.length());
  		client.println();
		client.println(payload);

		while (client.available()) {
			char c = client.read();
			Serial.write(c);
		}

		// delay(1000);
		Serial.println("Disconnecting from server.");
		client.stop();

	} else {
		Serial.println("Failed to connect to the server");
	}
}

float measureMoisture() {
	float moistureValue = float(analogRead(A1));
	return moistureValue;
}

float relativeMoisture(float moistureValue) {
	float moistureRelative = 1.0 - ((moistureValue - WaterValue) / amplitude);
	return moistureRelative;
}

void pumpWater(int duration) {
	Serial.println("Pumping water for " + String(duration) + " ms.");
	digitalWrite(1, LOW);
	delay(duration);
	digitalWrite(1, HIGH);
	Serial.println("Done!");
}

void setup() {
	//Initialize serial and wait for port to open:
	Serial.begin(9600);
	// while (!Serial) {
	// 	; // wait for serial port to connect. Needed for native USB port only
	// }
	delay(2000);

	// Set the water pump pin (1) to OUTPUT and switch it off
	pinMode(1, OUTPUT);
	digitalWrite(1, HIGH);

	connectWifi();
	printCurrentNet();
	printWiFiData();
}

void loop() {

	connectWifi();
	Serial.println();

	float moistureAbsolute = measureMoisture();
	Serial.print("Absolute moisture level: ");
	Serial.println(moistureAbsolute);
	float moistureRelative = relativeMoisture(moistureAbsolute);
	Serial.print("Relative moisture level: ");
	Serial.println(moistureRelative);

	Serial.print("Sending a POST request to ");
	Serial.println(server);

	// Make an HTTP request:

	// String moistureRelativeStr = String(moistureRelative, DEC);
	// String payload = String("{\"value\":\"" + moistureRelativeStr + "\"}");

	DynamicJsonDocument payload(1024);
	payload["moistureAbsolute"] = moistureAbsolute;
	payload["moistureRelative"] = moistureRelative;
	payload["AirValue"] = AirValue;
	payload["WaterValue"] = WaterValue;
	// payload["sensor"] = "gps";
	// payload["time"]   = 1351824120;
	// payload["data"][0] = 48.756080;
	// payload["data"][1] = 2.302038;

	String payloadStr;
	serializeJson(payload, payloadStr);

	sendData(payloadStr);

	if (moistureRelative < 0.25) {
		pumpWater(4000);
		Serial.println();
	}

	delay(60000);
}
